\documentclass[10pt]{article}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{url}
\usepackage{float}
\title{\textbf{Sentence prominence:\\Extracting relevant information for learning}}
\author{Matteo Francia\\matteo.francia2[at]studio.unibo.it}
\begin{document}
\maketitle
\begin{abstract}
Teaching a task to a robot is difficult. Automatic knowledge extraction can improve and simplify the learning phase. An approach based on sentence prominence is presented in this dissertation and finally is augmented with emotions.
\end{abstract}
\newpage
\tableofcontents
\newpage
\begin{center}
Copy-and-pasted citations are reported \textit{"in this way"}.
\end{center}
\section{Motivation}
Teaching a task to a robot is difficult. Interacting with a robot requires him/her to understand human actions and natural language. Automatic knowledge extraction can improve and simplify his/her learning phase. 

Research on infants demonstrated that language helps the children to structure the observations of the actions performed by the tutor. This phenomenon goes by the name of acoustic packaging \cite{hirsh1999origins}. An approach based on sentence prominence is presented in this dissertation and is finally augmented with emotions.

The robot is embodied in an environment in which events occurs along with speech signal. His/her goals are discovering the units of language, analysing events basing on language and connecting units of language with representation of the word.

The guide-idea of this dissertation is "How can a robot extract relevant information for learning?". Starting with an application scenario the importance of sentence prominence is then highlighted. State-of-art literature is summarized to support this thesis. Finally, emotions are imported into the model.

\subsection{Scenario}
\begin{figure}
\centering
\includegraphics[width=\textwidth]{img/cs1.png}
\caption{Experiments (a) and (b). (a) no sub-goal detection. (b) sub-goals are detected according to the robot primitives.}
\label{img:mot_scen_pc1}
\end{figure}

\begin{figure}
\includegraphics[width=\textwidth]{img/cs2.png}
\caption{Experiments (c) and (d). (c) sub-goal detection is based on utterances. (d) sub-goal detection is based on both acoustic and movement cues.}
\label{img:mot_scen_pc2}
\end{figure}
The source of this section is \cite{ugur2015use}. 

The goal of the proposed research is adding acoustic cues to achieve a natural human-robot interaction and studying the effects of combining speech and motion cues. 

A human is tutoring a robot demonstrating him/her cup-insertion, which is a simplified version of the cup stacking task. The robot should be capable of extracting automatically the information which were emphasized by the human. iCub is the robot employed in this experiment. He has already learned several manipulation primitives, namely grasping, lifting, carrying, dropping and pushing an object. His goal is to stack an object (a red puppet) inside a cup. 

The tutor exploits a learning by demonstration approach which relies on both visual and acoustic cues. The human combines pauses in the movement trajectory and speech utterances during the learning phase (see Figures \ref{img:mot_scen_pc1}, \ref{img:mot_scen_pc2}), leading to the segmentation of the demonstrations into sub-goals according to the robot's repertoire of actions. 

Highlights of the task, speech signal and object speed are reported in the "Demonstration" block. Emulation highlights are reported in "Imitation".

Sub-goals are detected by segmenting either object motion trajectory or speech activity. Motion segmentation is based on the speed of the object. The ongoing segment ends when a pause is detected. Speech activity segmentation exploits ESMERALDA. The experiment also involves a Kinect sensor, a microphone, and two objects placed on the table. The robot tracks objects in real-time using OpenCV library. 

\paragraph{Experiment (a) - without cues} iCub, without any cues, extracts only the initial and final states of the demonstrated action, moving the red object by pushing it to the right. The puppet pushes the box away rather than being inserted in it. The robot does not detect any sub-goals. 
\paragraph{Experiment (b) - motion cues}
The tutor inserts pauses in a rectangular trajectory without speech cues. 

iCub segments the trajectory and finds the sub-goals based on the pauses detected in the motion of the object. Emulation replays correctly the detected sub-goals. 
\paragraph{Experiment (c) - speech cues}
The tutor pronounces "up" and "down" during demonstration without introducing any pause while moving the object. 

The speech segmentation module segments the acoustic signal finding the sub-goals.  Emulation replays them correctly.  
\paragraph{Experiment (d) - motion and speech cues} The tutor picks up, shakes, and puts the object inside the container. While shaking, the humans pronounces "here". The utterance is detected by voice segmentation. 

Combining visual and speech sub-goals, iCub emulates the observed actions correctly. After the extraction of sub-goals represented in its perceptual space, the robot builds a sequence of behaviour from his repertoire.
\subsection{Automatic information extraction}
The automatic extraction of emphasized information is based on sentence prominence, which, according to \cite{kakouros2015automatic}, \textit{"is a property of speech [with which the] speaker can convey meaning [that is] not available in the linguistic message"}. 

This phenomenon is related to prosody that indeed \textit{"is the study of the tune and rhythm [aspects of] speech and how [they] contribute to meaning. [These aspects] apply to a level above [...] the individual phoneme and very often to sequences of words (in prosodic phrases)"} \cite{prosody}.

Prosody can change the meaning of a sentence relying on a speaker's attitude to what is being said (irony, sarcasm, ...). It also overlaps with emotions which affect the same acoustic features: intensity, vocal pitch, rhythm and rate of utterance. At the phonetic level, prosody is characterised by \cite{prosody}: fundamental frequency, loudness and rhythm (phoneme and syllable duration). Different approaches exist based on these features. 

The two following sections explain how sentence prominence and prosody are addressed in the state-of-art literature.

\section{Acoustic packaging system}
\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{img/ap.png}
\caption{Architecture of the system for acoustic packaging.}
\label{img:lit_ap_ap}
\end{figure}
The source of this subsection is \cite{schillingmann2011using}. However, only the information relevant to acoustic prominence and prosody detection are reported here.

Acoustic information, during a narration, overlaps with sequences of actions providing infants with a bottom-up guide to find structure within them. Hirsh-Pasek and Golinkoff introduced this combination as acoustic packaging \cite{hirsh1999origins}. In a tutoring scenario with infant learners, they are able to segment a multi-modal continuous stream into meaningful units basing on their perception and experience.

This approach exploits the fundamental frequency ($F0$) and the energy ($E$) of the acoustic signal. These feature are statistically modelled over time and combined with the information about the syllables to provide prominence estimations for each words composing the utterance. Variations in the acoustic physical properties are indicative of prominence implying stimulus-driven response of the listener to the parts perceived as more salient. In fact, an unexpected stimulus drives the listener's attention.

The acoustic package system (see Figure \ref{img:lit_ap_ap}) is composed by six modules which communicate through a central active memory. The memory notifies components when events occur. This solution allows the integration of modules and a decoupled design of the components. A feedback component is involved to highlight what the robot has understood from the demonstrations. The prominence detection module, as described in \cite{schillingmann2011using}, detects the information linguistically highlighted by the tutor. 


Three requirements has to be fulfilled:
\begin{enumerate}
\item Acoustic and temporal cue segmentation;
\item Temporal synchronization of sensory data;
\item On-line adaptation and feedback.
\end{enumerate} 

Speech plays a key role for the segmentation of a tutoring scenario as it helps the learner, namely the robot, to combine emphasized units with the action streaming. Sentence prominence identifies semantic information about the goal of the demonstration. The robot learns the actions basing on speech-visual analysis, as infants do. 

During the demonstration, iCub reacts to unexpected speech stimuli, focusing on utterances carrying paralinguistic information. Resulting in the segmentation of the task into well defined sub-goals. The feedback module replays the relevant parts of the utterance. 

Correction strategies have still to be implemented making the system to be capable to adapt to the tutor more accurately.

\section{Sentence prominence implementation}

A general work flow for sentence prominence is presented in the following section.
\subsection{General work flow}
\paragraph{1. Segmentation}
Speech stream is segmented into linguistic units. Often, the units corresponds to syllables since they are typically used in other detection methods and their segmentation do not require to use models with a known lexicon. 

Pre-processing (sampling, filtering, ...) of the signal may be required.

\paragraph{2. Prominence detection}
The choice of the acoustic features, on which the prominence detection algorithm is based, has to fulfil several requirements. Two of them are robustness to noise and fast implementation. The system should achieve high accuracy even in noisy scenarios and the extraction of the acoustic parameters should be fast enough for on-line learning and feedback.

According to the published approaches \cite{kakouros2015automatic, schillingmann2011using}, $F0$, $E$ and the syllable duration $D$ are considered relevant acoustic parameters. A speaker can modulate $F0$ and $E$ independently from the linguistic content of his/her message. The unpredictability of $F0$ trajectories is connected to human perception of sentence prominence. $D$ is a crucial prosodic cue, as stated in the above-mentioned definition of "prosody". A speaker conveys prominence to a word by stressing and unstressing the duration of each syllable.

After feature extraction, feature normalization may be required.


\paragraph{3. Evaluation}
The third step is rating the extracted units according to the mentioned acoustic parameters, resulting in syllable segments extended with prominence ratings. The new hypothesis are made available to other modules through the active memory component.

\paragraph{4. Testing}
The accuracy of the previous steps is estimated here. 

Human annotators can be asked for marking the most prominent parts of the speech. A cross-validation could be carried out between the syllables perceived as prominent by humans and the system. The accuracy is computed as a function of the number of matches. 

\subsection{CADSP}
The source of this subsection is \cite{kakouros2015automatic}. 

Speakers, in natural speech, emphasize some words more than others to draw the listener's attention to the most informative parts of the utterance. A speaker uses prosody to combine both linguistic and paralinguistic information facilitating listener's interpretation of the speech. The authors of \cite{kakouros2015automatic} presented an unsupervised approach for prominence detection which is inspired by human perception.

The algorithm is composed by two blocks (see Figure \ref{img:sp_cadsp}):
\begin{figure}
\centering
\includegraphics[width=\textwidth]{img/cadsp.png}
\caption{CADSP algorithm: overview of the processing steps in the algorithm.}
\label{img:sp_cadsp}
\end{figure}
\begin{itemize}
\item Block A: responsible for creating a statical model able to learn from a training set the trajectories of the prosodic features and then to produce probability estimation of data picked up from a training set (however, the corpora is not addressed in this dissertation);
\item Block B: responsible for the detection of syllabic nuclei.
\end{itemize}

\subsubsection{Block B}
\begin{figure}
\centering
\includegraphics[width=0.15\textwidth]{img/s1.png}
\caption{Estimation of the syllable structure. Syllabic nuclei correspond to envelope maxima located between the syllabic boundaries which are the local minima between the nuclei.}
\label{img:cadsp_blockb}
\end{figure}
Signal amplitude envelope is used to segment speech into syllables and to enumerate the syllabic nuclei in each word. The smoothed structure of the envelope is similar to the one of a syllable \cite{mermelstein1975automatic, villing2006performance}. Syllabic nuclei correspond to envelope maxima located between the syllabic boundaries which are the local minima between the nuclei.

To compute the envelope the absolute value of the speech signal is sampled at kHz, low-pass filtered with a 48-ms moving average filter and scaled to have a maximum value of one across the signal length. Syllabic boundaries are computed by simple minima detection. Boundaries closer than 80ms are collapsed in the midpoint. Finally, each local maximum between the detected boundaries is marked as a syllabic nucleus (see Figure \ref{img:cadsp_blockb}).

\subsubsection{Block A}
The goal of this block is marking words as prominence if the temporal evolution of the features trajectories violate the expectations of the listener during the words, capturing his/her attention. As mentioned before, the algorithm takes into account $F0$, $E$ and $D$ as they are the most descriptive features across several languages \cite{lieberman1960some, ortega2011acoustic}. 

This approach is based on n-gram model trained with $F0$ and $E$. $D$ is included in the model by integrating the probabilities of two features over word duration.

This model includes five processing steps consistent with the work flow proposed at the beginning of this section:

\paragraph{1. Pre-processing} Speech data are down-sampled to 1kHz. 

\paragraph{2. Feature extraction} $F0$ and $E$ are computed. $F0$ contours are extracted using the YAAPT algorithm \cite{zahorian2008spectral} with a 25-ms window and 10-ms step size. $E$ is computed using the same window length and step size:
\begin{equation}
E = \sum_{n=n_{1}}^{n_{2}}|x[n]|^2
\end{equation}
where $x$ is the speech input, $n_1$ and $n_2$ are respectively the beginning and the end of the window.
\paragraph{3. Feature normalization} This algorithm has to be robust with respect to the differences between speakers and spoken utterances. Min-max normalization is applied.
\begin{equation}
f'(t) = \dfrac{f(t) - minf(f)}{max(f) - min(f)}
\end{equation}
where $f$ is the feature value, $min(f)$ is the minimum value of the feature, $max(f)$ is the maximum value of the feature.
\paragraph{4. Quantization} Features are quantized into $Q$ discrete amplitude levels, $f'(t) \rightarrow a_t \in [1, 2, ..., Q]$ where $Q = 32$. 

Levels are computed using the k-means algorithm with a random sample initialization. The number of levels ranges between the least possible and the best contour approximation. 

\paragraph{5. n-gram training and probability emission}
n-gram probabilities are estimated from the relative frequencies of different n-tuples in the training data for different value of $n = 2, 3, 4$ tuning $F0$ and $E$.

At the beginning the n-gram probability is computed
\begin{equation}
P_\alpha=\frac{C_\alpha(a_t, ..., a_{t-n+1})}{C_\alpha(a_{t-1}, ..., a_{t-n+1})}
\end{equation}
where $C$ is n-tuple cardinality, $\alpha$ is the feature. 
The instantaneous feature log-probabilities are integrated for each n-gram assuming that they are independent each other
\begin{equation}
P'(t) = \sum_{\alpha} log(P_\alpha(a_t|a_{t-1}, ..., a_{t-n+1}))
\end{equation}

To estimate the prosody predictability during each word, the prominence score $S(w_{i,j})$ is computed integrating the instantaneous feature probabilities of $F0$ and $E$ over the duration of the entire word
\begin{equation}
S(w_ {i,j}) = \sum_{t=t_1}^{t_2}P'(t)
\end{equation}
where $j$ is the word index; $i$ is the utterance index, $t_1$ and $t_2$ are the word boundaries extracted from the corpora transcriptions, $t$ is the time.

As mentioned before stressing and unstressing syllables convey prosodic information to the linguistic content. Longer syllabic duration (unstressing) leads to increased perception of prominence. The exponential function is a good approximation of this aspect. 

\begin{equation}
S'(w_{i,j}) = S(w_{i,j}) e^{t_v}
\end{equation}
The average duration of the syllables within a word $t_v$ is computed by dividing the word duration $t_w$ (extracted from the corpora) by the number of nuclei $v$ contained in the word ($t_v=t_w/v$) which has been detected by "Block B". Each score $S'(w_{i,j})$ is weighted by $e^{t_v}$. 

Finally, the prominence level $H(w_{i,j})$ is estimated 
\begin{equation}
H(w_{i,j}) = \begin{cases} 
1, & \mbox{if } S'(w) < r_i \\ 
0, & \mbox{otherwise}
\end{cases}
\end{equation}
where $r_i$ is the threshold related to the sensitivity of prominence.

See Figure \ref{img:cadsp_s} for an example of the algorithm result.

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{img/s.png}
\caption{CADSP with the utterance "There is a clean yellow cow and a cookie". Top panel: original signal waveform, red marks is the word perceived as most prominent and yellow marks are the syllabic nuclei. Bottom panel: prominence score for each word.}
\label{img:cadsp_s}
\end{figure}

\section{Module for emotional prosody}
Part of this section is inspired by \cite{bozkurt2009improving, kehrein2002prosody}. 

The guideline of this section is understanding how emotion detection can improve sentence prominence for learning purpose. The goal is then to deploy a module to augment learning with emotions.

Emotions are crucial in both social and human-machine interaction. As reported in \cite{bozkurt2009improving} emotional content of speech has been focus for call center applications and for the development of toys able to emotionally enhance their interaction with children. 

Prosodic patterns differ according to the speaker's emotional states \cite{prosody} even though the exact relations between the two phenomena is partially unknown \cite{kehrein2002prosody}. For instance, some emotions can be expressed as a function of $F0$. A steep raising in the fundamental frequency, which correspond to an auditory shift in speech, makes the listener feel either shocked or surprised.

High values of pitch are correlated also with interjections, happiness, anger, and fear. On the other hand, low pitch values are associated with sadness and boredom. An increase in $D$ is often related to excitement, whereas a lower speech rate leads to the perception of a calm or demotivated attitude.

\textit{"Within an interaction, the attribution of emotions is inter-subjectively consistent. The perception of discrete emotions as semantically complex phenomena is based on a combination of all the signalling systems available during the communicative event in context"} \cite{kehrein2002prosody}. The proposed module has to address discrete emotions at different levels, such as speech, prosody, gestures, etc.. Only the acoustic feature will be considered in this dissertation.

The compositional approach to emotional meaning, proposed in \cite{kehrein2002prosody}, is based on prosodic features as they contribute to the perception of a variety of emotions. Thus the idea of augmenting learning with emotions through prosody detection seems to be a feasible idea.

It is assumed that emotions do not change over the word duration.

\subsection{Structure}
\begin{figure}
\centering
\includegraphics[width=0.6\textwidth]{img/e1.png}
\caption{Augmenting the acoustic packaging system with a module for emotion detection. $X$ extends the weighting feature of the CADSP algorithm.}
\end{figure}

The approach proposed here extends the system proposed before (see Figure \ref{img:lit_ap_ap}) with a new module for emotion detection. The outputs of both prominence detection $P$ and emotion detection $X$ are then aggregated by the function $f$ which emits an extended utterance hypothesis. The $P$ values are weighted by $X$.

Two weighting systems are proposed. Both extend the one proposed in the CADSP algorithm \cite{kakouros2015automatic}.
\subsubsection{Weigthting prosodic features with emotion cues}
Given $P'(t) = \sum_{\alpha} log(P_\alpha(a_t|a_{t-1}, ..., a_{t-n+1}))$ and $X_\alpha$ as parameters of the aggregator $f$, where $X_\alpha$ estimates how the feature $\alpha$ is weighted given emotion $X$ (sadness, happiness, boredom, ...), $P'_e(t)$ is computed as follow:

\begin{equation}
P'_e(t) = \sum_{\alpha} log(P_\alpha(a_t|a_{t-1}, ..., a_{t-n+1}) * X_\alpha)
\end{equation}

The following steps of the algorithm are then performed as usual.

\paragraph{Explanation} The emotion detection module extracts the most likely emotion among all the possible ones given $F0$, $E$ and $D$. However, discretizing the speaker's emotional state may not be easy as emotions are not objective.

Each emotion affects the prosodic features in a different way. Thus the evaluation of $P'_e(t)$ should take into account the effects of the emotion $X$ on the feature $\alpha$. For instance, when a person is bored the speech rate is lowered as well as the pith of $F0$. Given the longer duration of each syllable, even a small variation in speed, which otherwise would have been irrelevant in an apathetic scenario, has to be amplified by $X_\alpha$. 

For each emotion has to be provided a table containing the pairs (feature, weights):
\begin{center}
\begin{tabular}{| c |}
\hline
\textbf{Emotion $X$}\\\hline
\end{tabular}

\begin{tabular}{| c | c |}
\hline
$\alpha$ & $X_\alpha$ \\\hline
$F0$ & x \\\hline
$E$ & y \\\hline
$D$ & z \\\hline 
\end{tabular}
\end{center}
where the weights can be estimated by experts in the field of emotional prosody or approximated using machine learning algorithms.

\subsubsection{GMM}
Emotion and prosody can be statistically modelled using a Gaussian Mixture Model whom parameters are estimated using the Expectation-Maximization $EM$ algorithm. GMMs represent, indeed, parametric models of the probability distribution of continuous feature measurements, such as vocal-tract spectral features \cite{reynolds2015gaussian}. 

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{img/e2.png}
\caption{Example of 1-dimensional GMM. The likelihood $X$ is reported in the vertical axe. The feature $\alpha$ is reported in the horizontal one. Each Gaussian distribution $g$ represents the likelihood to observe a certain emotional value given the prosodic feature vector $\vec{x}$.} 
\label{img:e2}
\end{figure}

A dimension is added to the model for each feature $\alpha$. An example of 1-dimensional GMM is presented in Figure \ref{img:e2}. The likelihood $X$  is reported in the vertical axe. The feature $D$ is reported in the horizontal one. Each distribution $g$ represents the probability to observe an emotion given the the speech rate. The shorter is $D$, the more excited is the speaker (excitement could be the emotion represented with the red distribution).

The emotion for each word is estimated as follow
\begin{equation}
p(\mbox{emotional state}|\vec{x}) = \dfrac{\sum_{i=1}^{M} w_i g(\vec{x}|a_i, b_i)}{\mbox{normalization factor}}
\end{equation}
where $\vec{x}$ is the feature vector, $M$ is the number of considered emotions, $w_i$ is the weight associated with each distribution, $g$ is the emotion distribution, $a_i$ is the average value of the distribution, $b_i$ is the covariance of the distribution. 

Assuming that emotions which are not related to the prosodic features $\vec{x}$ will give an almost-null contribution to the sum, the likelihood of the emotional state is equal to the sum of the weighted distributions. This solve the problem of emotion discretization since all $g$s contribute to the sum. $p(\mbox{emotional state}|\vec{x})$ cannot exceed $1$, thus the constraint $p(\mbox{emotional state}|\vec{x}) \le 1$ requires to include a normalization factor in the above equation. 

$w_i$ is introduced to keep the model as general as possible. It is possible to consider each $w_i = 1, \forall i=1, ..., M$.

$P'_e(t)$ is computed as follow:
\begin{equation}
P'_e(t) = \sum_{\alpha} log(P_\alpha(a_t|a_{t-1}, ..., a_{t-n+1}) * p(\mbox{emotional state}|\vec{x}))
\end{equation}

The following steps of the algorithm are then computed as usual.


\section{Discussion}
This approach is inspired to the human perception of emotion and prosody. As shown by several studies \cite{prosody, kehrein2002prosody, mozziconacci2002prosody} prominence and emotions are strongly related. Augmenting the sentence prominence detection is a feasible solution which could simplify a human tutor to teach a task to a robot.

Without training and testing this system on a corpora, it is not possible to estimate the results empirically. In addition, this study is not suitable for speech including sarcasm or irony, as they require also to understand the linguistic content of the speech.

Tutoring is not the only possible application. Human social interaction in everyday-life implicitly involves emotions and body language. Combining these information with the prosodic cues could lead to new results in the human machine interaction field.


\bibliographystyle{unsrt}
\bibliography{asr} 
\end{document}