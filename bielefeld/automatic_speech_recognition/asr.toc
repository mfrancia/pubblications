\contentsline {section}{\numberline {1}Motivation}{3}
\contentsline {subsection}{\numberline {1.1}Scenario}{3}
\contentsline {paragraph}{Experiment (a) - without cues}{4}
\contentsline {paragraph}{Experiment (b) - motion cues}{5}
\contentsline {paragraph}{Experiment (c) - speech cues}{5}
\contentsline {paragraph}{Experiment (d) - motion and speech cues}{5}
\contentsline {subsection}{\numberline {1.2}Automatic information extraction}{5}
\contentsline {section}{\numberline {2}Acoustic packaging system}{5}
\contentsline {section}{\numberline {3}Sentence prominence implementation}{7}
\contentsline {subsection}{\numberline {3.1}General work flow}{7}
\contentsline {paragraph}{1. Segmentation}{7}
\contentsline {paragraph}{2. Prominence detection}{7}
\contentsline {paragraph}{3. Evaluation}{7}
\contentsline {paragraph}{4. Testing}{7}
\contentsline {subsection}{\numberline {3.2}CADSP}{8}
\contentsline {subsubsection}{\numberline {3.2.1}Block B}{8}
\contentsline {subsubsection}{\numberline {3.2.2}Block A}{9}
\contentsline {paragraph}{1. Pre-processing}{9}
\contentsline {paragraph}{2. Feature extraction}{9}
\contentsline {paragraph}{3. Feature normalization}{10}
\contentsline {paragraph}{4. Quantization}{10}
\contentsline {paragraph}{5. n-gram training and probability emission}{10}
\contentsline {section}{\numberline {4}Module for emotional prosody}{11}
\contentsline {subsection}{\numberline {4.1}Structure}{12}
\contentsline {subsubsection}{\numberline {4.1.1}Weigthting prosodic features with emotion cues}{13}
\contentsline {paragraph}{Explanation}{13}
\contentsline {subsubsection}{\numberline {4.1.2}GMM}{13}
\contentsline {section}{\numberline {5}Discussion}{15}
