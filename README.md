# README #

This README documents the structure of this repository

### What is this repository for? ###

* University dissertations, reports and presentations

# Sentence Prominence: Extracting relevant information for learning #

* bielefeld/automatic_speech_recognition/asr.pdf
## Abstract ##
Teaching a task to a robot is difficult. Automatic knowledge extraction can improve and simplify the learning phase. An approach based on sentence prominence is presented in this dissertation and finally is augmented with emotions.

### Professor ###
Britta Wrede, Recent trends in automatic speech recognition